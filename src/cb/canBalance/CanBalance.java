package cb.canBalance;
/*
 *	@see https://codingbat.com/prob/p158767
 * 
 */
public class CanBalance {

	public boolean canBalance(int[] nums) {
		int begin = 0;
		int sumLeft = 0;
		int sumRight = sumRightElementsArray(begin + 1, nums);
		if (nums.length == 1) {
			return false;
		}
		if (sumLeft == sumRight) {
			return true;
		}
		while (begin < nums.length) {
			if (sumLeft < sumRight) {
				sumLeft += nums[begin];
				if (begin + 1 < nums.length) {
					begin++;
				} else {
					break;
				}
				sumRight = sumRightElementsArray(begin, nums);
			} else if (sumLeft > sumRight) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	public int sumRightElementsArray(int start, int[] nums) {
		int sumRight = 0;
		for (int i = start; i < nums.length; i++) {
			sumRight += nums[i];
		}
		return sumRight;
	}
}
